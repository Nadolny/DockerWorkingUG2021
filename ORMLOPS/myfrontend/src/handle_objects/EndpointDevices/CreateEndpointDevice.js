/* eslint-disable */
import React, { useState, useEffect } from "react";
import axios from 'axios';

const PopulateEndpointDeviceTable = (props) => {
    //stany puste
    const [vehicle_serial_number, set_vehicle_serial_number] = useState([]);
    const [model_device_vehicle_codename, set_model_device_vehicle_codename] = useState([]);
    const [model_device_incremental_version_number, set_model_device_incremental_version_number] = useState([]);
    
    const handleAxiosPost= (event) => {
        event.preventDefault();
        // ma czekac na "onSubmit"
        // https://sebhastian.com/react-preventdefault/
        // https://www.javaguides.net/2020/08/reactjs-axios-get-post-put-and-delete-example-tutorial.html
        console.log(`Data to send ${vehicle_serial_number} ${model_device_vehicle_codename} ${model_device_incremental_version_number}`);
        axios.post('/endpoint_device', {
            vehicle_serial_number: vehicle_serial_number,
            model_device_vehicle_codename: model_device_vehicle_codename,
            model_device_incremental_version_number:  model_device_incremental_version_number,
        })
        // .then(response => console.log(response))
        .catch(error => console.log(error))

        const [localcounter, setLocalcounter] = useState(1);    
        //COUNTER https://reactjs.org/docs/hooks-state.html
        setLocalcounter(localcounter + 1);
        props.onChangeAxiosPost(localcounter)
    };
    return (
        <div>
            <h1>PopulateEndpointDeviceTable</h1>
            <label name="PopulateEndpointDeviceTable1">vehicle_serial_number:</label>
            <input id="PopulateEndpointDeviceTable1" type='text' value={vehicle_serial_number} onChange={event => set_vehicle_serial_number(event.target.value)} /><br />
            <label name="PopulateEndpointDeviceTable2">model_device_vehicle_codename:</label>
            <input id="PopulateEndpointDeviceTable2" type='text' value={model_device_vehicle_codename} onChange={event => set_model_device_vehicle_codename(event.target.value)} /><br />
            <label name="PopulateEndpointDeviceTable3">model_device_incremental_version_number:</label>
            <input id="PopulateEndpointDeviceTable3" type='numbers' value={model_device_incremental_version_number} onChange={event => set_model_device_incremental_version_number(event.target.value)} /><br />
            <input type='submit' value='Confirm' onClick={handleAxiosPost} />
        </div>
    );
};

export default PopulateEndpointDeviceTable;