import React, { useState, useEffect } from "react";
import axios from 'axios';

//props to jest właściwość https://pl.reactjs.org/docs/components-and-props.html
const AlterEndpointDevice = (props) => {
    const [id, setId] = useState([])
    const [vehicle_serial_number, setvehicle_serial_number] = useState([])
    const [model_device_vehicle_codename, setmodel_device_vehicle_codename] = useState([])
    const [model_device_incremental_version_number, setmodel_device_incremental_version_number] = useState([])
    //local counters
    const [getcountn, setcountn] = useState(1);
    const [somecounter, setSomecounter] = useState(1);

    //useEffect() hook accepts 2 arguments:
    //useEffect(callback[, dependencies]);
    useEffect( () => {
        setvehicle_serial_number(props.currentEndpointDevice.vehicle_serial_number);
        setmodel_device_vehicle_codename(props.currentEndpointDevice.model_device_vehicle_codename);
        setmodel_device_incremental_version_number(props.currentEndpointDevice.model_device_incremental_version_number);
    }, [props.currentEndpointDevice]
    );

    //
    //za https://developer.allegro.pl/about/
    //HTTP PUT: Wykorzystywana do edycji zasobu
    //https://www.javaguides.net/2020/08/reactjs-axios-get-post-put-and-delete-example-tutorial.html
    const handlePropoundModification = (event) => {
        axios.put(`/endpoint_device/${props.currentEndpointDevice.id}`, {
            vehicle_serial_number: vehicle_serial_number,
            model_device_vehicle_codename: model_device_vehicle_codename,
            model_device_incremental_version_number: model_device_incremental_version_number,
        })
            //.then(response => console.log(response))
            .catch(error => console.log(error));

        setcountn(getcountn + 1);
        props.changecurrentEndpointDevice(getcountn)
        setvehicle_serial_number("");
        setmodel_device_vehicle_codename("");
        setmodel_device_incremental_version_number("");
        event.preventDefault();
    };

    const onInputChangevehicle_serial_number = (event) => {
        setvehicle_serial_number(event.target.value);
        event.preventDefault();
    };
    const onInputChangemodel_device_vehicle_codename = (event) => {
        setmodel_device_vehicle_codename(event.target.value);
        event.preventDefault();
    };
    const onInputChangemodel_device_incremental_version_number = (event) => {
        setmodel_device_incremental_version_number(event.target.value);
        event.preventDefault();
    };

    const handlePropoundErase = (event) => {
        axios.delete(`/endpoint_device/${props.currentEndpointDevice.id}`, {
        })
          //  .then(response => console.log(response))
            .catch(error => console.log(error));   
        setSomecounter(somecounter + 1);
        props.changeParentHandler4(somecounter);
    }

    return (
        <div>
            <h1>Alter Endpoint Device </h1>
            <label >id:</label>
            <input id='AlterEndpointDevicetable1' type='number' name="id" defaultValue={props.currentEndpointDevice.id} onChangeInitialValueEvent={event => setId(event.target.value)} /><br />
            <label >VehicleSerialNumber:</label>
            <input id='AlterEndpointDevicetable2' type='text' name="vehicle_serial_number" value={vehicle_serial_number} onChangeInitialValueEvent={onInputChangevehicle_serial_number} /><br />
            <label >ModelDeviceVehicleCodename:</label>
            <input id='AlterEndpointDevicetable3' type='text' name="model_device_vehicle_codename" value={model_device_vehicle_codename} onChangeInitialValueEvent={onInputChangemodel_device_vehicle_codename} /><br />
            <label >ModelDevicIncrementalVersionNumber:</label>
            <input id="AlterEndpointDevicetable4" type='number' name="model_device_incremental_version_number" value={model_device_vehicle_codename} onChangeInitialValueEvent={onInputChangemodel_device_incremental_version_number} /><br />         
            <input type='submit' value='SAVE' onClick={handlePropoundModification}/><input type='submit' value='DELETE' onClick={handlePropoundErase} />
        </div>
    );
};

export default AlterEndpointDevice;