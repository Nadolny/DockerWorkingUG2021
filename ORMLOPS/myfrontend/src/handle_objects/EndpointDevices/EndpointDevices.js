import React, { useState, useEffect } from "react";
import axios from 'axios';

const EndpointDevices = (props) => {
    const [EndpointDevices, setEndpointDevices] = useState([]);
    //const [EndpointDevice, setEndpointDevice] = useState([]);

        //useEffect() hook accepts 2 arguments:
        //useEffect(callback[, dependencies]);
        //https://www.javaguides.net/2020/08/reactjs-axios-get-post-put-and-delete-example-tutorial.html
    useEffect( () => {
            const fetchData = async () => {
                axios.get('/endpoint_device')
                    .then(response => setEndpointDevices(response.data))
                    .catch(error => console.log(error));
            };
            const timer = setTimeout(() => {
                fetchData();
            }, 100);
            return () => clearTimeout(timer);
            }, [props.onChange1, props.onChange2, props.onChange3]
    );
                    {/*&emsp; tabulatory*/}
    return (
        //fragment drzewa (tree fragment reactjs) rootelement można wstawić div ale trzeba by nadac klase dla przejrzystosci
            <div className="EndpointDevices">
                <h1>EndpointDevices</h1>
                {EndpointDevices.map(EndpointDevice => (<div key={EndpointDevices.id} >
                Id:&emsp;{EndpointDevice.id}&emsp;
                VehicleSerialNumber:&emsp;{EndpointDevice.vehicle_serial_number}&emsp;
                ModelDeviceVehicleCodename:&emsp;{EndpointDevice.model_device_vehicle_codename}&emsp;
                ModelDeviceIncrementalVersionNumber:&emsp;{EndpointDevice.model_device_incremental_version_number}&emsp;
                <button class="button" onClick={() => { props.onChangeByButtonEndpointDevice(EndpointDevice) }}>Apply</button>&emsp;
            </div>))}
            </div>

    );
};


export default EndpointDevices;