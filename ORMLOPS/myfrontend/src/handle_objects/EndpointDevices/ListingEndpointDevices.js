import React, { useState } from 'react';

import PopulateEndpointDeviceTable from './handle_objects/CreateEndpointDevice';
import AlterEndpointDevice from './handle_objects/AlterEndpointDevice';
import EndpointDevices from './handle_objects/EndpointDevices';

//import Post from "./Post";
//import MyForm from "./MyForm";

//aby funkcja była "hookable" musi sie zaczynac z dużej litery
function Function_EndpointDevices(props) {

  //get set
  const [originalStatePopulate, setOriginalStatePopulate] = useState(1);
  const [originalStateAlter, setOriginalStateAlter] = useState(1);

  const [currentEndpointDevice, setCurrentEndpointDevice] = useState(1)
  const [deletedEndpointDevice, setDeletedEndpointDevice] = useState(1)

  const InitialValueEvent = (event) => {
   setOriginalStatePopulate(event.target.value)
   event.preventDefault();
  };
  return (
  <div className="EndpointDevicesDataManipulation">
  {/* {initialValue} <br /> <input onChange={handleInitialValue}/>
  <Post noPosts= {initialValue} changeNoPosts={setInitialValue}/> */} 
  <input onChangeInitialValueEvent={InitialValueEvent}/>
  {/*https://reactjs.org/docs/components-and-props.html*/}
  <EndpointDevices onChange1={originalStatePopulate} onChange2={originalStateAlter} onChange3={deletedEndpointDevice} onChangeByButtonEndpointDevice={setCurrentEndpointDevice} />
  <PopulateEndpointDeviceTable onChangeAxiosPost={setOriginalStatePopulate} />
  <AlterEndpointDevice currentEndpointDevice={currentEndpointDevice} changecurrentEndpointDevice={setOriginalStateAlter} changeParentHandler4={setDeletedEndpointDevice} />
  {/*<MyForm /> */}
  </div>
  );

}

export default Function_EndpointDevices;