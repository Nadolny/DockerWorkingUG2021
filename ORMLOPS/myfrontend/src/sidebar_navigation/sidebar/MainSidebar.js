import React from 'react';
//import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

import "./MainSidebar.css";
/* eslint-disable */


//import Endpoints from '../component/Management/Endpoints';



const MainSidebar = ({ MainSidebarOpen, closeMainSidebar }) => {
  return (
    <div className={MainSidebarOpen ? "main_sidebar_responsive" : ""} id="main_sidebar">
      <div className="main_sidebar__title">
        <div className="main_sidebar__img">
          <h1>ORMLOPS component manager</h1>
        </div>
        <i
          onClick={() => closeMainSidebar()}
          className="fa fa-align-justify"
          id="sidebarIcon"
          aria-hidden="true"
        ></i>
      </div>

      <div className="main_sidebar__menu">
        <div className="main_sidebar__link active_menu_link">
          <i className="fa fa-superpowers"></i>
          <a href="#">Control Panel</a>
        </div>
        <h2>Management</h2>
        <div className="main_sidebar__link">
          <i className="fa fa-user-secret" aria-hidden="true"></i>
          <a href="#">Admin</a>
        </div>
        <div className="main_sidebar__link">
          <i class="fa fa-wrench"></i>
          <a href="#">Endpoints</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-wrench"></i>
          <a href="#">Clusters</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-server"></i>
          <a href="#">Data Warehouse</a>
        </div>
        
       
       
        <h2>Models</h2>
        <div className="main_sidebar__link">
          <i className="fa fa-money"></i>
          <a href="#">Blueprints</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-university"></i>
          <a href="#">Training</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-tasks"></i>
          <a href="#">Testing</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-truck"></i>
          <a href="#">Pipelines</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-thermometer-2"></i>
          <a href="#">Monitoring</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-circle-o-notch"></i>
          <a href="#">Continuous Integration</a>
        </div>


        <h2>PREDICTIONS</h2>
        <div className="main_sidebar__link">
          <i className="fa fa-question"></i>
          <a href="#">Evaluation</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-sign-out"></i>
          <a href="#">Possible</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-retweet"></i>
          <a href="#">Remodelling</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-files-o"></i>
          <a href="#">Almost matches</a>
        </div>

        <div className="main_sidebar__link">
          <i className="fa fa-sitemap"></i>
          <a href="#">To integrate</a>
        </div>


      
        <h2>CLUSTERS</h2>
        <div className="main_sidebar__link">
          <i className="fa fa-cubes"></i>
          <a href="#">Kubernetes</a>
        </div>
        <div className="main_sidebar__link">
          <i className="fa fa-briefcase"></i>
          <a href="#">SpecialOps</a>
        </div>
        <div className="main_sidebar__logout">
          <i className="fa fa-power-off"></i>
          <a href="#">Log out</a>
        </div>
      </div>
    </div>
  );
};

export default MainSidebar;
