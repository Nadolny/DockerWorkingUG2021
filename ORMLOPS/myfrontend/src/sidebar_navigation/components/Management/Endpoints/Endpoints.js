import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import "./contol_panel.css";
//import Chart from "../charts/Chart";





function Endpoints  ()  {
  return (
    <main>
      <div className="main__container">
        {/* <!-- MAIN TITLE STARTS HERE --> */}

        <div className="main__title">
          
          <div className="main__greeting">
            <h1>ORMLOPS Endpoint devices Management</h1>
            <p>manage servers or robots individually </p>
          </div>
        </div>

        {/* <!-- MAIN TITLE ENDS HERE --> */}

        {/* <!-- MAIN CARDS STARTS HERE --> */}
        <div className="main__cards">

          <div className="card">
            <i
              className="fa fa-magic fa-2x text-lightblue"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Current deployments</p>
              <span className="font-bold text-title">45</span>
            </div>
          </div>

          <div className="card">
            <i className="fa fa-thumbs-up fa-2x text-green" aria-hidden="true"></i>
            <div className="card_inner">
              <p className="text-primary-p">Active endpoint devices</p>
              <span className="font-bold text-title">312</span>
            </div>
          </div>

        
          
          <div className="card">
            <i
              className="fa fa-code fa-2x text-yellow"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Current editions</p>
              <span className="font-bold text-title">12</span>
            </div>
          </div>

          <div className="card">
            <i
              className="fa fa-space-shuttle fa-2x text-red"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Active drone fleets</p>
              <span className="font-bold text-title">3</span>
            </div>
          </div>
        </div>
        {/* <!-- MAIN CARDS ENDS HERE --> */}

        {/* <!-- CHARTS STARTS HERE --> */}
        <div className="charts">
          <div className="charts__left">
            <div className="charts__left__title">
              <div>
                <h1>Eficiency statistics</h1>
                <p>do the things go right or not?</p>
              </div>
              <i className="fa fa-bar-chart" aria-hidden="true"></i>
            </div>
            {/*<!-- <Chart /> --> */}
          </div>

          <div className="charts__right">
            <div className="charts__right__title">
              <div>
                <h1>Statistics</h1>
                <p>Manage wisely</p>
              </div>
              <i className="fa fa-bar-chart" aria-hidden="true"></i>
            </div>

            <div className="charts__right__cards">
              <div className="card1">
                <h1>Finished </h1>
                <p>23</p>
              </div>

              <div className="card2">
                <h1>Active nodes</h1>
                <p>375</p>
              </div>

              <div className="card3">
                <h1>All models</h1>
                <p>63</p>
              </div>

              <div className="card3">
              <h1>Algorithm families</h1>
                <p>9</p>
              </div>

              <div className="card4">
                <h1>Errors</h1>
                <p>13</p>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- CHARTS ENDS HERE --> */}
      </div>
    </main>
  );
};

export default ControlPanel;
