import "./Navbar.css";
/* eslint-disable */

const MainNavbar = ({ MainSidebarOpen, openMainSidebar }) => {
  return (
    <nav className="navbar">
      <div className="nav_icon" onClick={() => openMainSidebar()}>
        <i className="fa fa-bars" aria-hidden="true"></i>
      </div>
      <div className="navbar__left">
    
        <a className="active_link" href="#">
          Control Panel
        </a>
         
        
        <a href="#">Projects</a>
         
        <a href="#">Datasets</a>
      
        <a href="#">Models</a>
         
        <a href="#">Experiments</a>
         
        <a href="#">Deployments & Apps</a>
         
        <a href="#">Repo</a>
         
        <a href="#">Wiki</a>
      </div>
      <div className="navbar__right">
        <a href="#">
          <i className="fa fa-search" aria-hidden="true"></i>
        </a>
        <a href="#">
          <i className="fa fa-user-circle " aria-hidden="true"></i>
        </a>
        
      </div>
    </nav>
  );
};

export default MainNavbar;
