
//import logo from './logo.svg';
import './App.css';

//import Post from "./Post";
//import MyForm from "./MyForm";
import ControlPanel from "./sidebar_navigation/main/control_panel";
import MainNavbar from "./sidebar_navigation/navbar/Navbar";
import MainSidebar from "./sidebar_navigation/sidebar/MainSidebar";


import { useState } from "react";




//function stateless react
function App() {



  const [MainSidebarOpen, setMainSidebarOpen] = useState(true);


  const openMainSidebar = () => {
    setMainSidebarOpen(true);
  }


  const closeMainSidebar = () => {
    setMainSidebarOpen(false);
  }

  return (
    <div className="container">
      
      
      <MainNavbar MainSidebarOpen={MainSidebarOpen} openMainSidebar={openMainSidebar} />
      <ControlPanel />
      <MainSidebar MainSidebarOpen={MainSidebarOpen} closeMainSidebar={closeMainSidebar} />
    </div>

    

  );
}

export default App;


