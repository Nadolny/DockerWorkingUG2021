
#to edit 

#!/usr/bin/env bash


kubectl apply -f ./namespace-kubernetes/ormlops-namespace.yml #OK
echo "\e[1;45mUtworzono przestrzeń nazw ORMLOPS"
# kubectl apply -f ./my-app-config.yml #edit check czy inni używają konfigmapy reckony tak
echo "\e[1;45mwyświetlenie obecnych w systemie k8s przestrzeni nazw"
kubectl get namespace


 kubectl apply -f ./mypostgres-kubernetes/pv-local.yml # OK
echo -e "\e[0;40mUtworzono wolumin persystentny\e[1;m"
 kubectl apply -f ./mypostgres-kubernetes/postgres-pvc.yml #OK
echo -e "\e[0;40mUtworzono claim do woluminu persystentnego\e[1;m"

kubectl create -f ./myredis-kubernetes/redis-clusterip.yml # OK
echo -e "\e[1;41mUtworzono serwis ClusterIP dla redis\e[1;m"
kubectl create -f ./myredis-kubernetes/redis-configMap.yml # OK
echo -e "\e[1;41mUtworzono serwis configMap dla redis\e[1;m"
kubectl create -f ./myredis-kubernetes/redis-deployment.yml # OK
echo -e "\e[1;41mUtworzono deployment dla redis\e[1;m"

kubectl apply -f ./mypostgres-kubernetes/postgres-clusterip.yml #OK
echo -e "\e[0;44mUtworzono serwis ClusterIP dla postgres\e[1;m"
kubectl apply -f ./mypostgres-kubernetes/postgres-secret.yml #OK
echo -e "\e[0;44mUtworzono secret dla postgres\e[1;m"
kubectl apply -f ./mypostgres-kubernetes/postgres-configMap.yml #OK
echo -e "\e[0;44mUtworzono serwis configMap dla postgres\e[1;m"
kubectl apply -f ./mypostgres-kubernetes/postgres-deployment.yml #OK
echo -e "\e[0;44mUtworzono deployment dla postgres\e[1;m"


kubectl apply -f ./mybackend-kubernetes/backend-NodePort.yml #OK
echo -e "\e[0;46mUtworzono NodePort dla backend\e[1;m"
kubectl apply -f ./mybackend-kubernetes/backend-clusterip.yml #OK
echo -e "\e[0;46mUtworzono serwis ClusterIP dla backend\e[1;m"
kubectl apply -f ./mybackend-kubernetes/backend-deployment.yml #OK
echo -e "\e[0;46mUtworzono deployment dla backendu\e[1;m"


kubectl create -f ./myfrontend-kubernetes/frontend-clusterip.yml  #OK 
echo -e "\e[1;42mUtworzono serwis ClusterIP dla frontendu produkcyjnego hostowanego przez nginx\e[1;m"
kubectl apply -f ./myfrontend-kubernetes/frontend-NodePort.yml #OK
echo -e "\e[1;42mUtworzono NodePort dla dla frontendu produkcyjnego hostowanego przez nginx\e[1;m"
kubectl create -f ./myfrontend-kubernetes/frontend-deployment.yml  #OK 
echo -e "\e[1;42mUtworzono deployment dla frontendu produkcyjnego hostowanego przez nginx\e[1;m"

# kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.47.0/deploy/static/provider/cloud/deploy.yaml

kubectl apply -f ./ingress-kubernetes/deploy_ingress_controller_v0.47.yml # OK
echo -e "\e[1;45mUruchamianie lub pobieranie kontrolera dla Ingress v0.47, to może chwilę potrwać\e[1;m"
kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=40s
echo -e "\e[1;45mUtworzono kontroler dla Ingress\e[1;m"

kubectl create -f ./ingress-kubernetes/myapp-ingress.yml
echo -e "\e[1;45mUtworzono service Ingress\e[1;m"



echo -e "\e[1;33mLista deployment przestrzeni nazw ORMLOPS:"
kubectl get deployment --namespace ormlops-namespace


echo -e "\e[1;33mLista podów przestrzeni nazw ORMLOPS:"
kubectl get pods --namespace=ormlops-namespace