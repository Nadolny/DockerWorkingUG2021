# UTWORZENIE NAMESPACE
# kubectl create namespace ormlops-namespace
# kubectl run nginx --image=nginx --namespace=ormlops-namespace

# kubectl get namespace # ns alias namespace 
# kubectl delete namespace ormlops-namespace

# zmienić na te nazwy serwisów tworzonych 

echo -e "\e[1;41mUsuwanie przestrzeni nazw ORMLOPS może chwilę potrwać prosimy o cierpliwość\e[1;m"
kubectl delete namespace ormlops-namespace
echo -e "\e[1;41mUsunięto przestrzeń nazw ORMLOPS\e[1;m"
echo -e "\e[1;41mUsuwanie przestrzeni nazw Ingress może chwilę potrwać prosimy o cierpliwość\e[1;m"
kubectl delete namespace ingress-nginx
echo -e "\e[1;41mUsunięto przestrzeń nazw Ingress\e[1;m"
kubectl delete services --all
echo -e "\e[1;41mUsunięto serwisy\e[1;m"
kubectl delete configMaps --all
echo -e "\e[1;41mUsunięto wszystkie configMapy\e[1;m"
kubectl delete deployments --all
echo -e "\e[1;41mUsunięto wszystkie deploymenty\e[1;m"
kubectl delete pods --all
echo -e "\e[1;41mUsunięto wszystkie pody\e[1;m"
kubectl delete ingresses --all
echo -e "\e[1;41mUsunięto wszystkie serwisy typu Ingress\e[1;m"
kubectl delete pvc --all
echo -e "\e[1;41mUsunięto wszystkie Persistent Volume Claimy\e[1;m"
#kubectl delete pv --all
#echo -e "\e[1;41mUsunięto wszystkie Persystentne Woluminy\e[1;m"
kubectl delete secret --all
echo -e "\e[1;41mUsunięto wszystkie wszystkie sekrety\e[1;m"


