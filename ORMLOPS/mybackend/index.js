const express = require('express');
const cors = require('cors')
const {v4 : uuidv4} = require('uuid');
//dodac uuid
const bodyParser = require("body-parser");
const { Pool } = require('pg');



const expressapp = express();
const router = express.Router();

module.exports = router;


////API PORT
const PORT = 5000;
expressapp.listen(PORT, () => {
  console.log(`API listening on port ${PORT}`);
})




expressapp.use(cors());
expressapp.use(express.json());




//REDIS

const redis = require('redis');

const redisClient = redis.createClient({
  host: "myredis",
  port: 6379
  //retry_strategy: () => 1000
});

redisClient.on('connect', () => {
  console.log("Connected to Redis cache server");
});

redisClient.on('error', (error) => {
  console.log(error);
});




//POSTGRES DB

const pgClientPool = new Pool({
  user: "postgres",
  password: "1qaz2wsx",
  database: "myappdb",
  host: "mypostgres",
  port: "5432"
});

pgClientPool.on('error', () => {
  console.log("Postgres not connected");
});


//ENDPOINT DEVICE
pgClientPool
  .query('CREATE TABLE IF NOT EXISTS endpoint_device ( id SERIAL PRIMARY KEY, vehicle_serial_number VARCHAR(255), model_device_vehicle_codename VARCHAR(50), model_device_incremental_version_number NUMERIC )')
  .catch((error) => {
    console.log(error);
  });


pgClientPool
  .query('CREATE TABLE IF NOT EXISTS jamming_table ( id SERIAL PRIMARY KEY, gps_jamming VARCHAR(255), gps_jamming_contra_working VARCHAR(255), radio_jamming_detected VARCHAR(255), radio_jamming_contra_working VARCHAR(255), geographic_indicator_parameter VARCHAR(255))')
  .catch((error) => {
    console.log(error);
  });


pgClientPool
  .query('CREATE TABLE IF NOT EXISTS world_environment_tested (id SERIAL PRIMARY KEY, sands_tested VARCHAR(255), watering_failure_tested VARCHAR(255), planned_water_landing_tested VARCHAR(255')
  .catch((error) => {
    console.log(error);
  });


pgClientPool
  .query('CREATE TABLE IF NOT EXISTS mlops_tested_on_device (id SERIAL PRIMARY KEY, vehicle_serial_number VARCHAR(255), model_device_vehicle_codename VARCHAR(50), model_device_incremental_version_number NUMERIC, data_model_tool VARCHAR(100), data_model_tool_version VARCHAR(20), data_model_codename VARCHAR(255), tested_catalog_tree_on_device VARCHAR(5))')
  .catch((error) => {
    console.log(error);
  });


pgClientPool
  .query('CREATE TABLE IF NOT EXISTS uploading_history_table (id SERIAL PRIMARY KEY datastamp_of_uploading VARCHAR(255), device_of_uploading VARCHAR(50), model_device_incremental_version_number NUMERIC, data_model_tool VARCHAR(100), data_model_tool_version VARCHAR(20), data_model_codename VARCHAR(255), tested_catalog_tree_on_device VARCHAR(5))')
  .catch((error) => {
    console.log(error);
  });

  




expressapp.use(bodyParser.urlencoded({
  extended: true
}));
expressapp.use(bodyParser.json());


//ENDPOINT DEVICE - manipulation on device which residues in a database

//HTTP 204 No Content success status response code indicates that a request has succeeded, but that the client doesn't need to navigate away from its current page.
//The successful result of a PUT or a DELETE is often not a 200 OK but a 204
//(or a 201 Created when the resource is uploaded for the first time).

const get_endpoint_device = (request, response) => {
  try {
    console.log(request.body)
    pgClientPool.query('SELECT * FROM endpoint_device', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
      console.log('get_endpoint_device OK')
    })
  } catch (error) {
    console.log(error)
  }
};

const create_endpoint_device = (request, response) => {
  try {
    console.log(request.body)
    const { vehicle_serial_number, model_device_vehicle_codename, model_device_incremental_version_number } = request.body
    pgClientPool.query('INSERT INTO endpoint_device (vehicle_serial_number, model_device_vehicle_codename,  model_device_incremental_version_number) VALUES ($1, $2, $3)', [vehicle_serial_number, model_device_vehicle_codename, model_device_incremental_version_number], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`endpoint_device became added`)
      console.log('create_endpoint_device OK')

    })
  } catch (error) {
    console.log(error)
  }
};



const update_endpoint_device = (request, response) => {
  try {
    const id = parseInt(request.params.id);
    console.log(request.body)
    const { vehicle_serial_number, model_device_vehicle_codename, model_device_incremental_version_number } = request.body


    pgClientPool.query(`UPDATE endpoint_device SET vehicle_serial_number=$1, model_device_vehicle_codename=$2, model_device_incremental_version_number=$3 WHERE id=${id}`, [vehicle_serial_number, model_device_vehicle_codename, model_device_incremental_version_number], (error, results) => {
      if (error) {
        throw error
      }
      response.status(204).send(`endpoint_device became updated`)
      console.log('update_endpoint_device OK')

    })
  } catch (error) {
    console.log(error)
  }
};

const delete_endpoint_device = (request, response) => {
  try {
    const remove_vehicle_id = parseInt(request.params.remove_vehicle_id);
    console.log(request.body)

    pgClient.query(`DELETE FROM endpoint_device WHERE id=${remove_vehicle_id}`, (error, results) => {
      if (error) {
        throw error
      }
      response.status(204).send(`endpoint_device deleted with id: ${remove_vehicle_id}`)
      console.log('delete_endpoint_device OK')
    })
  } catch (error) {
    console.log(error)
  }
};

//HTTP 200 OK success status response code indicates that the request has succeeded
const get_endpoint_device_by_vehicle_serial_number = (request, response) => {
  try {
    const vehicle_serial_number = request.params.vehicle_serial_number;

//  sprawdzanie czy obiekt jest w redisClient

    redisClientPool.get(vehicle_serial_number, async (error, endpoint_device) => {
      if (endpoint_device) {
        return res.status(200).send({
          error: false,
          message: `Loading endpoint_device from the Redis cache`, 
          data: JSON.parse(endpoint_device)
        })
      
      } else {
        console.log(`endpoint_device is not in Redis `)
        pgClientPool.query(`SELECT * FROM endpoint_device WHERE vehicle_serial_number = $1`, [vehicle_serial_number], (error, results) => {
          if (error) {
            throw error
          }
          redisClient.setex(vehicle_serial_number, 1440, JSON.stringify(results.rows));
          response.status(200).json(results.rows)
          console.log('Loading from the Postgres and adding to Redis cache')
        })

      }
    })
  } catch (error) {
    console.log(error)
  }
};


/*

const db = new pg.Client({ database: 'articles' });

db.connect((err, client) => {
  if (err) throw err;
  console.log('Connected to database', db.database);

*/



expressapp.get('/', (request, response) => response.send('express server is working'))
expressapp.get('/endpoint_device/', get_endpoint_device)
expressapp.post('/endpoint_device/', create_endpoint_device)
expressapp.put('/endpoint_device/:id/', update_endpoint_device)
expressapp.delete('/endpoint_device/:remove_vehicle_id/', delete_endpoint_device)
expressapp.get('/endpoint_device/byvehicle_serial_number/:vehicle_serial_number', get_endpoint_device_by_vehicle_serial_number)




