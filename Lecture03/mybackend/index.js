//index.js


const express = require('express');
//const bodyParser = require('body-parser'); BODY PARSER IS DEPRECATED
const cors = require('cors');



const app = express();

app.use(cors());
//app.use(bodyParser.json()); ; BODY PARSER IS DEPRECATED
app.use(express.json());

const redis = require('redis');

const redisClient = redis.createClient({ 
	host: "myredis",
	port: 6379,
	retry_strategy: () => 1000
});


redisClient.on('connect', () => {
	console.log("Connected to redis server");
});

const {Pool} = require ('pg');

//user:"utworz sobie jakiegos innego uzytkownika tutaj niz domyslny postgres"
const pgClient = new Pool({
	user:"postgres",
	password: "1qaz2wsx",
	database:"postgres",
	host: "mypostgres",
	port: "5432"
});


pgClient.on('error', () =>{
	console.log("Postgres not connected");
});

pgClient
.query('CREATE TABLE IF NOT EXISTS numbers (number INT)')
.catch( (err) => {
	console.log(err);
});

//tutaj dokoncz z nagrania


app.get("/", (req, res) => {
	res.send("Hello world")
});


const PORT = 5000;
app.listen(PORT, () => {
	console.log(`API listening on port ${PORT}`);
});
