
const express = require('express');
//const bodyParser = require('body-parser'); BODY PARSER IS DEPRECATED
const cors = require('cors');



const app = express();

app.use(cors());
//app.use(bodyParser.json()); ; BODY PARSER IS DEPRECATED
app.use(express.json());

const redis = require('redis');

const redisClient = redis.createClient({ 
	host: "myredis",
	port: 6379,
	//retry_strategy: () => 1000
});

redisClient.on('connect', () => {
	console.log("Connected to redis server");
});


app.get("/", (req, res) => {
	res.send("Hello world")
});

const PORT = 5000;
app.listen(PORT, () => {
	console.log(`API listening on port ${PORT}`);
});
