import React, {useState,useEffect} from "react";
import axios from 'axios';


const Post = (props) => {

    const [] = useState([]); //initialState
    const [number, setNumber]
    useEffect( () => {
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then(response => setPosts(response.data))
        .catch(error => console.log(error)); //consola przegladarki
    }, []);


    const handlePostClick =(event) => {
    console.log(event.target)
    }

    const handleNumberChange = (event) => {
        setNumber(event.target.value);
        props.changeNoPosts(event.target.value);
    };


    return (
        //fragment drzewa (tree fragment reactjs) rootelement można wstawić div ale trzeba by nadac klase dla przejrzystosci
        <> 
            <div>
                {posts 
                // .filter(post=> post,title.startsWith('a'))
                .slice (0, props.noPosts)
                .map(post => (<div key={post.id} onClick={handlePostClick}>{post.title}</div>))} 
            </div>
            <div>
                <div>Number {number}</div>
                <input onChange={handleNumberChange}/>
            </div>
        </>
    //post uuid zamiast post id to bledne rozwiazania
    )
}

export default Post;